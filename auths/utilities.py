from core.models import ClientAccount
from django.utils.crypto import get_random_string


def check_password_validity(password1, password2):
    return False if password1 != password2 else True


def generate_user_id(client):
    first_name = client.owner.first_name
    last_name = client.owner.last_name

    user_id = first_name+'_'+last_name

    if ClientAccount.objects.filter(user_id=user_id).exists():
        user_id += get_random_string(5)
        if ClientAccount.objects.filter(user_id=user_id).exists():
            user_id += get_random_string(5)

        return user_id
    else:
        return user_id







