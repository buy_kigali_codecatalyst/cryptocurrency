from django.urls import path
from . import views
from django.contrib.auth.views import LogoutView

urlpatterns = [
    path('create_account', views.CreateClientAccount.as_view()),
    path('activate/<str:token>/', views.activate_account, name='activate'),

    path('login', views.LoginView.as_view()),
    path('logout', LogoutView.as_view()),

]
