import datetime
from django.shortcuts import render
from django.template.loader import render_to_string
from core.models import PasswordToken
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .serializers import *

from django.contrib.auth import authenticate
from django.utils import timezone
from django.utils.crypto import get_random_string
from django.core.mail import send_mail
from cryptocurrency.settings import EMAIL_HOST_USER
from .models import CustomUser
from .utilities import  check_password_validity, generate_user_id
from rest_framework.authtoken.models import Token
from core.models import ClientAccount


class CreateClientAccount(APIView):
    def post(self, request):
        form = CustomUserSerializer(data=request.data)
        if form.is_valid():
            email = form.data.get('email')
            first_name = form.data.get('first_name')
            last_name = form.data.get('last_name')
            password = form.data.get('password')
            confirm_password = form.data.get('confirm_password')

            if check_password_validity(password, confirm_password) is False:
                return Response({"errors": [{"password": "passwords do not match"}]}, status=400)

            user = None

            try:
                user = CustomUser.objects.get(email=email)
            except:
                pass

            if user is None:
                try:
                    user = CustomUser.objects.create_user(email=email, password=password, first_name=first_name,
                                                          last_name=last_name, is_active=False
                                                          )
                except:
                    # return Response({"errors": [{"email": "duplicate email"}]})
                    return Response({"errors": [{"email": "duplicate email"}]}, status=400)

                client = ClientAccount.objects.create(owner=user)
                client.user_id = generate_user_id(client)
                client.save()
            if PasswordToken.objects.filter(user=user).exists():
                pass
            else:
                token = get_random_string(length=25)
                PasswordToken.objects.create(user=user, code=token,
                                             expire_date=timezone.now() + datetime.timedelta(days=1))
            client = ClientAccount.objects.get(owner=user)

            # scheme = request.META.get('wsgi.url_scheme')
            subject = "Activate Your Account"
            to_email = [email]
            message = render_to_string('activate_account.html', {
                'user': user,
                'domain': request.META['HTTP_HOST'],
                'token': PasswordToken.objects.get(user=user).code,

            })
            try:
                send_mail(subject, message, EMAIL_HOST_USER, to_email, fail_silently=False, html_message=message)
            except:
                return Response({"errors": [{"email": "The email wasn't sent, pleas try again latter"}]}, status=500)
            return Response({"client": DisplayClientSerializer(client).data,
                             "message": "Thank you for creating your account!! check your email to  activate your account.\n "
                                        "if you don't see your email, check your spam"}, status=200)

        else:
            return Response({"errors": form.errors}, status=400)


def activate_account(request, token):
    try:
        # print(token)
        password_token = PasswordToken.objects.get(code=token)
        user = password_token.user
        user.is_active = True
        user.save()
        existing_tokens = PasswordToken.objects.filter(user=user)
        existing_tokens.delete()
        return render(request, "activated.html")

    except Exception:

        return Response({"error": 'Activation link is invalid!'})


class LoginView(APIView):
    def post(self, request):
        try:
            email = request.data.get('email')
            password = request.data.get('password')

            if password is None or email is None:
                return Response({"errors": [{"you did not provide your email/password"}]},
                                status=400)
            else:
                user = authenticate(email=email, password=password)
                if user is not None:
                    # login(request, user)

                    user.last_login = timezone.now()

                    user.save()
                    token, created = Token.objects.get_or_create(user=user)
                    client = ClientAccount.objects.get(owner=user)
                    return Response({"client": DisplayClientSerializer(client).data,
                                     "token": token.key})
                else:
                    return Response({"errors": [{"message": "invalid credentials"}]},
                                    status=401)
        except Exception as e:
            return Response({"errors": str(e)})
