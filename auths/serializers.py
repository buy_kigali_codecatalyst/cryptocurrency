from rest_framework import serializers
from core.models import ClientAccount
from auths.models import CustomUser


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['email', 'first_name', 'last_name', 'is_active', 'last_login', 'user_type']


class CustomUserSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    email = serializers.EmailField(max_length=30)
    first_name = serializers.CharField(max_length=30)
    last_name = serializers.CharField(max_length=30)
    password = serializers.CharField(max_length=30)
    confirm_password = serializers.CharField(max_length=30)


class OwnerSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ["first_name", "last_name", "email"]


class DisplayClientSerializer(serializers.ModelSerializer):
    owner = OwnerSerializer()

    class Meta:
        model = ClientAccount
        fields = ["id", "owner", "user_id", "balance"]
