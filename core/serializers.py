from rest_framework import serializers
from .models import ClientAccount, CoinsRequest
from auths.serializers import DisplayClientSerializer


class TransactionSerializer(serializers.Serializer):
    receiver_id = serializers.IntegerField(write_only=True)
    amount = serializers.FloatField()

    def get_receiver(self):
        try:
            client = ClientAccount.objects.get(id=self.validated_data.get('receiver_id'))
            return client
        except:
            raise Exception("No client account found!")


class RequestCoinsSerializer(serializers.Serializer):
    amount = serializers.FloatField(default=0.0)


class CoinsRequestsDisplaySerializer(serializers.ModelSerializer):
    member = DisplayClientSerializer()
    class Meta:
        model = CoinsRequest
        fields = "__all__"