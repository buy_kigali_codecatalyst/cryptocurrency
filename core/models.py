import datetime
from django.db import models
from auths.models import CustomUser
from cryptocurrency import settings
from .utils import update_hash
from django.utils import timezone
from django.utils.crypto import get_random_string


# utilities


def check_balance(client):
    chain = client.chain
    client.balance = 0
    client.save()
    blocks = Block.objects.filter(chain=chain)
    for block in blocks:
        data = block.data.split('-->')
        if str(client.id) == data[0]:
            client.balance -= float(data[2])
        elif str(client.id) == data[1]:
            client.balance += float(data[2])
    client.save()
    return client.balance


class ClientAccount(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True)
    user_id = models.CharField(max_length=250, default="")
    balance = models.FloatField(default=0, null=True)
    """ selling price for each XB coin in dollars """
    selling_price = models.FloatField(default=0.0)
    chain = models.ForeignKey(to='BlockChain', on_delete=models.SET_NULL, null=True)

    def __str__(self):
        return 'ANONYMOUS' if self.owner is None else self.owner.first_name

    def save(self, *args, **kwargs):
        chains = BlockChain.objects.all()
        if chains.exists():
            chain = chains[0]
        else:
            chain = BlockChain.objects.create()
        self.chain = chain
        super(ClientAccount, self).save(*args, **kwargs)


class PasswordToken(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    code = models.CharField(max_length=250)
    expire_date = models.DateField(default=timezone.now() + datetime.timedelta(days=1))

    def check_if_valid(self):
        if self.expire_date > timezone.now().date():
            return False
        else:
            return True


class Transaction(models.Model):
    sender = models.ForeignKey(ClientAccount, on_delete=models.SET_NULL, null=True, related_name='sender_transactions')
    receiver = models.ForeignKey(ClientAccount, on_delete=models.SET_NULL, null=True,
                                 related_name='receiver_transactions')
    amount = models.FloatField(default=0)
    time_stamp = models.DateTimeField(auto_now_add=True)
    transaction_id = models.CharField(max_length=255, blank=True)
    added_to_block = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.sender.owner.first_name} Transferred {self.amount} coins to {self.receiver.owner.first_name}"

    def save(self, *args, **kwargs):
        if self.added_to_block is True:
            pass
        else:
            if check_balance(self.sender) < self.amount and self.sender.owner.user_type != 'ADMIN':
                raise Exception("No enough money!")
            elif self.sender is self.receiver or self.amount <= 0.00:
                raise Exception("Invalid transaction!")

            block_chain = BlockChain.objects.all()[0]
            data = f"{self.sender.id}-->{self.receiver.id}-->{self.amount}"
            block_chain.mine(Block(data=data))
            self.added_to_block = True
            self.transaction_id = get_random_string(25)
        super(Transaction, self).save(*args, **kwargs)


class Block(models.Model):
    time_stamp = models.DateTimeField(default=timezone.now())
    data = models.TextField(blank=True, max_length=255)
    hash = models.CharField(max_length=255, blank=True)
    previous_hash = models.CharField(max_length=255, default="0" * 64)
    chain = models.ForeignKey(to='BlockChain', on_delete=models.CASCADE)
    nonce = models.CharField(max_length=255, default=0, blank=True)

    def hash_(self):
        return update_hash(
            self.pk,
            self.data,
            self.previous_hash,
            self.nonce)

    def __str__(self):
        return str(f"Block#: {self.pk}\n "
                   f"Data: {self.data}\n "
                   f"Previous hash: {self.previous_hash}\n "
                   f"Nonce: {self.nonce}\n "
                   f"Created on: {self.time_stamp}")


class BlockChain(models.Model):
    time_stamp = models.DateTimeField(default=timezone.now())
    difficulty = models.IntegerField(default=3)

    def add_block(self, block):
        block.chain = self
        block.hash = block.hash_()
        block.save()

    def mine(self, block):
        blocks = Block.objects.filter(chain=self)
        if blocks.exists():
            block.previous_hash = blocks.last().hash

        while True:
            if block.hash_()[:self.difficulty] == "0" * int(self.difficulty):
                self.add_block(block)
                break
            else:
                block.nonce += 1

    def check_if_valid(self):
        chains = Block.objects.filter(chain=self)
        for i in range(1, len(chains)):
            _previous = chains[i].previous_hash
            _current = chains[i - 1].hash

            if _previous != _current or _current != "0" * self.difficulty:
                return False

        return True

class CoinsRequest(models.Model):
    member = models.ForeignKey(ClientAccount, on_delete=models.CASCADE)
    amount = models.FloatField(default=0.0)
    payed = models.BooleanField(default=False)


