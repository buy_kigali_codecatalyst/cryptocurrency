from django.urls import path
from . import views

urlpatterns = [
    path('bank_distribute_coins', views.bank_distribute_coins),
    path('member_send_coins', views.member_transfer_coins),
    path('check_balance', views.check_balance_view),
    path('request_coins', views.request_coins_from_bank),
    path('display_requests', views.display_requests),

    path('search_client_by_user_id/<str:user_id>', views.search_client_by_user_id),
    path('search_client_by_names/<str:name>', views.search_client_by_names),

]
