from django.contrib.auth.decorators import login_required
from rest_framework import status

from rest_framework.response import Response

from .models import *
from .serializers import *
from .utilities import check_if_admin_decorator, check_if_user_decorator, check_if_logged_in
from rest_framework.response import Response
from rest_framework.decorators import api_view
from auths.serializers import *
from django.db.models import Q


@api_view(['POST'])
@check_if_admin_decorator
def bank_distribute_coins(request):
    if request.method == 'POST':
        try:
            form = TransactionSerializer(data=request.data)
            if form.is_valid():
                bank = ClientAccount.objects.get(owner=request.user)
                client = form.get_receiver()
                Transaction.objects.create(sender=bank, receiver=client, amount=form.data.get('amount'))
                return Response({"message": "Transaction done"})
            else:
                return Response({"errors": form.errors}, status=400)
        except Exception as e:
            print(str(e))
            return Response({"errors": str(e)}, status=500)


@api_view(['POST'])
@check_if_user_decorator
def request_coins_from_bank(request):
    client = ClientAccount.objects.get(owner=request.user)
    if request.method == 'POST':
        form = RequestCoinsSerializer(data=request.data)
        if form.is_valid():
            CoinsRequest.objects.create(member=client, amount=form.data.get('amount'), payed=True)
            your_requests = CoinsRequest.objects.filter(member=client)

            return Response({"message": "created", "requests": CoinsRequestsDisplaySerializer(your_requests, many=True).data})
        else:
            return Response({"errors": form.errors}, status=400)
    else:
        your_requests = CoinsRequest.objects.filter(member=client)
        return Response({"message": "created", "requests": CoinsRequestsDisplaySerializer(your_requests, many=True).data})


@api_view(['GET'])
@check_if_admin_decorator
def display_requests(request):
    members_requests = CoinsRequest.objects.filter(payed=True)
    return Response({"requests": CoinsRequestsDisplaySerializer(members_requests, many=True).data})


@api_view(['POST'])
@check_if_user_decorator
def member_transfer_coins(request):
    if request.method == 'POST':
        try:
            form = TransactionSerializer(data=request.data)
            if form.is_valid():
                sender = ClientAccount.objects.get(owner=request.user)
                receiver = form.get_receiver()
                # print(client)
                Transaction.objects.create(sender=sender, receiver=receiver, amount=form.data.get('amount'))
                return Response({"message": "Transaction done"})
            else:
                return Response({"errors": form.errors}, status=400)
        except Exception as e:
            print(str(e))
            return Response({"errors": str(e)}, status=500)


@api_view(['GET'])
@check_if_logged_in
def check_balance_view(request):
    if request.method == 'GET':
        client_account = ClientAccount.objects.get(owner=request.user)
        return Response({"balance": check_balance(client_account)})


def show_transactions(request):
    pass


@api_view(('GET',))
def search_client_by_user_id(request, user_id):
    try:
        client = ClientAccount.objects.get(user_id=user_id)
        serializer = DisplayClientSerializer(client, many=False)
        return Response({'client': serializer.data}, status=status.HTTP_200_OK)
    except Exception:
        return Response({'error': [{'message': 'client not found'}]}, status=status.HTTP_404_NOT_FOUND)


@api_view(('GET',))
def search_client_by_names(request, name):
    try:
        clients = ClientAccount.objects.filter(
            Q(owner__first_name=name) | Q(owner__last_name=name))

        clientSerializer = DisplayClientSerializer(clients, many=True)
        return Response({'message': clientSerializer.data}, status=status.HTTP_200_OK)
    except Exception as e:
        return Response({'errors': [{'message': str(e)}]}, status=status.HTTP_400_BAD_REQUEST)


