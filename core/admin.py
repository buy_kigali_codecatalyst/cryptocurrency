from django.contrib import admin
from .models import *


# Register your models here.
class AdminPasswordToken(admin.ModelAdmin):
    list_display = ['user', 'code', 'expire_date']


class AdminClientAccount(admin.ModelAdmin):
    list_display = ['owner', 'user_id', 'balance']


admin.site.register(ClientAccount, AdminClientAccount)
admin.site.register(PasswordToken, AdminPasswordToken)
admin.site.register(Block)
admin.site.register(BlockChain)
admin.site.register(Transaction)
admin.site.register(CoinsRequest)
