
from django.core.exceptions import PermissionDenied
from django.utils.crypto import get_random_string

from auths.models import CustomUser
from rest_framework.response import Response

from core.models import ClientAccount


def check_if_logged_in(function):
    def wrap(request, *args, **kwargs):
        user = request.user
        if user.is_anonymous:
            raise PermissionDenied
        else:
            return function(request, *args, **kwargs)

    return wrap


def check_if_admin_decorator(function):
    def wrap(request, *args, **kwargs):
        user = request.user
        if user.is_anonymous:
            return Response({"errors": [{"Authorization": "Login first!"}]}, status=401)
        if user.user_type == 'ADMIN':
            return function(request, *args, **kwargs)
        else:
            return Response({"errors": [{"Authorization": "You are not allowed to perform this action"}]}, status=401)

    return wrap


def check_if_user_decorator(function):
    def wrap(request, *args, **kwargs):

        user = request.user
        if user.is_anonymous:
            return Response({"errors": [{"Authorization": "Login first!"}]}, status=401)
        if user.user_type == "CLIENT":
            return function(request, *args, **kwargs)
        else:
            return Response({"errors": [{"Authorization": "You are not allowed to perform this action"}]}, status=401)

    return wrap


def check_if_admin(user):
    if user.user_type == "ADMIN":
        return True
    else:
        return False



