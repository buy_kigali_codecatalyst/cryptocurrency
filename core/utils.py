from hashlib import sha256


def update_hash(*args):
    hash_text = ''
    h = sha256()
    for arg in args:
        hash_text += str(arg)

    h.update(hash_text.encode('utf-8'))
    return h.hexdigest()




